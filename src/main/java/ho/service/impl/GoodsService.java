package ho.service.impl;

import ho.entity.SecondGoods;
import ho.mapper.GoodsDao;
import ho.vo.GoodsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class GoodsService {
	
	@Autowired
	GoodsDao goodsDao;
	
	public List<GoodsVo> listGoodsVo(){
		return goodsDao.listGoodsVo();
	}

	public GoodsVo getGoodsVoByGoodsId(long goodsId) {
		return goodsDao.getGoodsVoByGoodsId(goodsId);
	}

	public boolean reduceStock(GoodsVo goods) {
		SecondGoods g = new SecondGoods();
		g.setGoodsId(goods.getId());
		int res = goodsDao.reduceStock(g);
		return res > 0;
	}
	
	public void resetStock(List<GoodsVo> goodsList) {
		for(GoodsVo goods : goodsList ) {
			SecondGoods g = new SecondGoods();
			g.setGoodsId(goods.getId());
			g.setStockCount(goods.getStockCount());
			goodsDao.resetStock(g);
		}
	}

	public int redisToMysql(Integer count,Long goodId){
		return goodsDao.redisDataToMysql(count,goodId);
	}
	
}
