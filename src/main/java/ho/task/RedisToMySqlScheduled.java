package ho.task;

import ho.redis.GoodsKey;
import ho.service.impl.GoodsService;
import ho.util.RedisUtil;
import ho.vo.GoodsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class RedisToMySqlScheduled implements ApplicationRunner {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private RedisUtil redisUtil;

    private Map<Long,Boolean> map = new ConcurrentHashMap<>();

    @Scheduled(cron = "15 05 11 * * ? ")
    public void redisDataToMySql(){

        if (!map.isEmpty()){
            map.entrySet().forEach(entry->{

                Integer count = redisUtil.get(GoodsKey.getSecondKillGoodsStock, ":" + entry.getKey(), Integer.class);
                goodsService.redisToMysql(count,entry.getKey());
            });
        }

    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        List<GoodsVo> goodsVos = goodsService.listGoodsVo();

        goodsVos.forEach(e->{
            if (Objects.nonNull(e)){
                map.put(e.getId(),false);
            }

        });

    }
}
