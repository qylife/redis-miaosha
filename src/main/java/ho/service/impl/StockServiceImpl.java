package ho.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ho.entity.Stock;
import ho.mapper.StockMapper;
import ho.service.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cc
 * @since 2022-06-09
 */
@Service
public class StockServiceImpl extends ServiceImpl<StockMapper, Stock> implements IStockService {

    @Autowired
    private StockMapper stockMapper;

    @Override
    public boolean updateStock(Long productId, Integer version) {
        return stockMapper.updateStock(productId,version);
    }
}
