package ho.handler;

import ho.constant.RankConstant;
import ho.service.impl.GoodsService;
import ho.vo.GoodsVo;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.connection.RedisZSetCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Component
public class RankInit implements ApplicationRunner {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private RedisTemplate redisTemplate;


    @Override
    public void run(ApplicationArguments args) throws Exception {

        //redisTemplate.delete(RankConstant.RankKey);

        List<GoodsVo> goodsVos = goodsService.listGoodsVo();

        goodsVos.forEach(e->{

            Long rank = redisTemplate.opsForZSet().reverseRank(RankConstant.RankKey, String.valueOf(e.getId()));

            if (Objects.isNull(rank)){
                redisTemplate.opsForZSet().add(RankConstant.RankKey,String.valueOf(e.getId()),0);
            }

        });
    }


}
