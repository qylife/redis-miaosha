package ho.util;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ho.redis.KeyPrefix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class RedisUtil {

    @Autowired
    private RedisTemplate redisTemplate;

    public <T> boolean set(KeyPrefix prefix, String key, T value) {

        try {
            String str = beanToString(value);
            if (StringUtils.isEmpty(str)){
                return false;
            }
            //GoodKey:prefix+key 类名:prefix+key
            String realKey = prefix.getPrefix() + key;
            System.out.println("realKey:"+realKey);
            if (prefix.expireSeconds()<=0){
                redisTemplate.opsForValue().set(realKey,str);
            }else {
                redisTemplate.opsForValue().set(realKey,str,prefix.expireSeconds(), TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public <T> T get(KeyPrefix prefix, String key, Class<T> clazz) {

        try {
            String realKey = prefix.getPrefix() + key;
            System.out.println("realKey:"+realKey);
            String str =(String)redisTemplate.opsForValue().get(realKey);
            T t = stringToBean(str, clazz);
            return t;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T stringToBean(String str, Class<T> clazz) throws JsonProcessingException {
        if (str == null || str.length() == 0 || clazz == null) {
            return null;
        }
        if (clazz == int.class || clazz == Integer.class) {
            return (T) Integer.valueOf(str);
        } else if (clazz == String.class) {
            return (T) str;
        } else if (clazz == long.class || clazz == Long.class) {
            return (T) Long.valueOf(str);
        } else {
            return new ObjectMapper().readValue(str,clazz);
        }
    }

    public static <T> String beanToString(T value) throws JsonProcessingException {
        if (value == null) {
            return null;
        }
        Class<?> clazz = value.getClass();
        if (clazz == int.class || clazz == Integer.class) {
            return "" + value;
        } else if (clazz == String.class) {
            return (String) value;
        } else if (clazz == long.class || clazz == Long.class) {
            return "" + value;
        } else {
            return new ObjectMapper().writeValueAsString(value);
        }
    }

}
