package ho.mapper;


import ho.entity.SecondGoods;
import ho.vo.GoodsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;


@Mapper
public interface GoodsDao {
	
	@Select("select g.*,sg.stock_count, sg.start_date, sg.end_date,sg.second_price from second_goods sg left join goods g on sg.goods_id = g.id")
	public List<GoodsVo> listGoodsVo();

	@Select("select g.*,sg.stock_count, sg.start_date, sg.end_date,sg.second_price from second_goods sg left join goods g on sg.goods_id = g.id where g.id = #{goodsId}")
	public GoodsVo getGoodsVoByGoodsId(@Param("goodsId")long goodsId);

	@Update("update second_goods set stock_count = stock_count - 1 where goods_id = #{goodsId} and stock_count > 0")
	public int reduceStock(SecondGoods g);
	
	@Update("update second_goods set stock_count = #{stockCount} where goods_id = #{goodsId}")
	public int resetStock(SecondGoods g);

	@Update("update second_goods set stock_count = #{stockCount} where goods_id = #{goodsId}")
	int redisDataToMysql(@Param("stockCount")Integer stockCount,@Param("goodsId")Long goodsId);
	
}
