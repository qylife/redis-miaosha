package ho.redis;

public class GoodsKey extends BasePrefix {

	private GoodsKey(int expireSeconds, String prefix) {
		super(expireSeconds, prefix);
	}

	private GoodsKey(String prefix) {
		super(prefix);
	}

	public GoodsKey(int expireSeconds, boolean addRandom, String prefix) {

		super(expireSeconds, addRandom, prefix);
	}

	public static GoodsKey getSecondKillGoodsStock = new GoodsKey("gs");

}
