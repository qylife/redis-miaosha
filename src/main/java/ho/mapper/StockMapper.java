package ho.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ho.entity.Stock;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cc
 * @since 2022-06-09
 */
public interface StockMapper extends BaseMapper<Stock> {

    boolean updateStock(@Param("productId") Long productId,@Param("version") Integer version);
}
