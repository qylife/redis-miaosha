package ho.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import ho.entity.Stock;
import ho.service.IStockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/stock")
@Slf4j
public class StockController {

    @Autowired
    private IStockService iStockService;


    @GetMapping("one")
    public Stock one(@RequestParam Long id){

        return iStockService.getById(id);
    }


}
