package ho.redis;

public interface KeyPrefix {
		
	int expireSeconds();
	
	String getPrefix();
	
}
