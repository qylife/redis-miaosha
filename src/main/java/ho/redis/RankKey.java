package ho.redis;

public class RankKey extends BasePrefix{


    public RankKey(int expireSeconds, boolean addRandom, String prefix) {
        super(expireSeconds, addRandom, prefix);
    }

    public RankKey(String prefix) {
        super(prefix);
    }

    public RankKey(int expireSeconds, String prefix) {
        super(expireSeconds, prefix);
    }


}
