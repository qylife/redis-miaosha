package ho.entity;

import lombok.Data;

import java.util.Date;

@Data
public class SecondGoods {
	private Long id;
	private Long goodsId;
	private Integer stockCount;
	private Date startDate;
	private Date endDate;
}
