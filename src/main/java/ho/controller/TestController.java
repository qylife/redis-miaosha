package ho.controller;


import ho.constant.RankConstant;
import ho.redis.GoodsKey;
import ho.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RequestMapping("redis")
@RestController
public class TestController {

    @Autowired
    private RedisUtil redisUtil;
    
    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("set")
    public void set(){

        redisUtil.set(new GoodsKey(60,true,"gl"),"",String.class);

    }

    @GetMapping("get")
    public void get(){

        String gl = redisUtil.get(new GoodsKey(60, true, "gl"), "", String.class);
        System.out.println("gl:"+gl);

    }

    @RequestMapping("easyRank")
    public Set easyRank(){

        return this.redisTemplate.opsForZSet().reverseRangeWithScores(RankConstant.RankKey, 0, 4);

    }

    @RequestMapping("addScore")
    public String addScore(@RequestParam Long goodId){

        this.redisTemplate.opsForZSet().incrementScore(RankConstant.RankKey,String.valueOf(goodId),1);
        return "ok";
    }

}
