package ho.service;

import com.baomidou.mybatisplus.extension.service.IService;
import ho.entity.Stock;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cc
 * @since 2022-06-09
 */
public interface IStockService extends IService<Stock> {

    boolean updateStock(Long productId, Integer version);
}
